<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Undergold</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
    <script src="js/index.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VT54XBLLH2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VT54XBLLH2');
    </script>
</head>

<body style="background: black">


    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="info_no_exclusivos">
            <center>

                <h5 style="  font-weight: lighter;margin-top:36px; color:white;">AÚN NO HACES PARTE DE
                    NUESTROS
                    <b id="letra_mas_grande">EXCLUSIVE MEMBERS.</b>
                </h5>
                <br><br>
                <h5 style="font-weight: lighter; color:white; ">PARA ADQUIRIR ESTA MASTERPIECE ,
                    DEBERÁS ESPERAR AL LANZAMIENTO DE LA PRÓXIMA COLECCIÓN.
                </h5>
                <br><br>
                <h5 style=" font-weight: lighter; color:white; ">
                    CONOCE LOS REQUISITOS PARA SER PARTE DE NUESTROS EXCLUSIVE MEMBERS Y OBTENER MÁS BENEFICIOS.
                </h5>
                <br><br>
                <a href="requisitos" class="form-control" id="buttom_consultar">REQUISITOS</a>
                <br><br>
                <img src="img/logo_3.png" alt="">
            </center>
        </div>
    </div>


</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
</script>

</html>