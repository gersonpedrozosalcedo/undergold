<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Undergold</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
    <script src="js/index.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VT54XBLLH2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VT54XBLLH2');
    </script>
</head>

<body style="background: black">


    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="info_requisitos">
            <center>

                <h5 style="  font-weight: lighter;margin-top:36px; color:white;">NUESTROS MEJORES Y MÁS
                    FIELES CLIENTE CUMPLE CON LOS SIGUIENTE CRITERIOS:

                </h5>
                <br><br>
                <h5 style=" font-weight: lighter; color:white; ">
                    • DEBEN HABER REALIZADO UN MÍNIMO DE 4 COMPRAS EN LOS ÚLTIMOS 120 DÍAS.
                    <br>
                    • SUS FACTURAS DEBEN SUMAR COMO MÍNIMO UN $1.000.000 DE PESOS.
                </h5>

                <br><br>
                <h5 style="  font-weight: lighter; color:white; ">
                    ESTA LISTA SE ACTUALIZA CADA MES, SI AÚN NO PERTENECES A
                    NUESTROS <b id="letra_mas_grande">EXCLUSIVE MEMBERS</b>, PODRÁS SERLO CUMPLIENDO
                    ESTOS CRITERIOS PARA EL PRÓXIMO MES.

                </h5>

                <br><br>
                <img src="img/logo_3.png" alt="">
            </center>
        </div>
    </div>


</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
</script>

</html>