<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Undergold</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link href="https://fonts.cdnfonts.com/css/bahnschrift" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/index.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VT54XBLLH2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VT54XBLLH2');
    </script>
</head>

<body style="background: black;">
    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="">

            <center><img src="img/logo_blanco.png" alt=""></center>
            <br><br><br>
            <center><img src="img/text1.png" id="text1" alt=""></center>
            <br><br><br>
            <center>
                <form id="form_cedula">
                    <input type=" text" id="campo_cedula" name="cedula" class="form-control"
                        placeholder="Ingrese su número de cédula">
                    <br><br>
                    <a href="javascript:void(0);" class="form-control" id="buttom_consultar"
                        onclick="consultar()">CONSULTAR</a>
                </form>
                <br>
                <p id="mensaje_consultando"></p>
            </center>
            <hr>
            <center>
                <p>© Copyright 2023 </p>
            </center>
        </div>
    </div>
</body>

<script src=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
</script>

</html>