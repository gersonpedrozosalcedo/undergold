function consultar() {
  var cedula = document.getElementById("campo_cedula").value;

  if (cedula === "") {
    $("#mensaje_consultando").html("Por favor ingrese un número de cédula.");
  } else {
    var url_peticion = "../actions/consultar_cedula_action.php";

    $.ajax({
      type: "GET",
      url: url_peticion,
      data: $("#form_cedula").serialize(),
      beforeSend: function () {
        $("#mensaje_consultando").html("Cargando, por favor espere...");
      },
      success: function (data) {
        $("#mensaje_consultando").html(data);
      },
      error: function () {
        alert("Por favor vuelvelo a intentar más tarde");
      },
    });
  }
}
