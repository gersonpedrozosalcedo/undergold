<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Undergold</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="stylesheet" href="css/style.css">
    <script src="js/index.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-VT54XBLLH2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-VT54XBLLH2');
    </script>
</head>

<body style="background: #f1f1f1">


    <div class="d-flex align-items-center justify-content-center vh-100">
        <div class="info_exclusivos">
            <center>
                <!-- <img src="img/logo_firma.png" style="width: 30%" alt="">-->
                <br>
                <br>
                <h5 style="font-size: 17px;  font-weight: lighter;margin-top:19px">GRACIAS POR SE PARTE DE NUESTROS
                    <br> <b id="letra_mas_grande">EXCLUSIVE MEMBERS.</b>
                </h5>
                <br>
                <br>
                <h5 style="font-size: 17px;  font-weight: lighter; margin-top:-1px">DISFRUTA DE TODOS LOS
                    <b id="letra_mas_grande">BENEFICIOS ÚNICOS</b> QUE <br> TENEMOS PREPARADOS PARA TI.
                </h5>

                <br>
                <img src="img/logo_firma.png" style="width: 50%" alt="">

                <br>
                <br>
                <img src="img/Undergold_2.png" id="logo_exclusive" style="width: 138px; margin-left: -39px;" alt="">
            </center>
        </div>
    </div>


</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous">
</script>

</html>